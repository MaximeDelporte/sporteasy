import Foundation
import RxCocoa
import RxSwift
import SwiftyJSON

class CalendarRepository {
  
  func getCalendarEvents() -> Observable<[CalendarEvent]> {
    let request = ApiRequest(.get, "/default/events")
    return Api.main.request(request).map { response in
      let json = response.json["events"]
      return json.arrayValue.map { CalendarEvent(json: $0) }
    }
  }
  
  func createCalendarEvent(with params: Api.Params) -> Observable<[CalendarEvent]> {
    let request = ApiRequest(.post, "/default/events", params: params)
    return Api.main.request(request).map { response in
      let json = self.getJSON(fromFile: "post-calendar-event")["events"] // response.json["events"]
      return json.arrayValue.map { CalendarEvent(json: $0) }
    }
  }
}

extension CalendarRepository {
  func getJSON(fromFile path: String) -> JSON {
    let bundle = Bundle(for: type(of: self))
    let url    = bundle.url(forResource: path, withExtension: "json")!
    let data   = try! Data(contentsOf: url)
    return JSON(parseJSON: data.string!)
  }
}
