import Foundation
import SwiftyJSON

// TODO : - Check if we can init in init (params)

class CalendarEvent {
  
  var leftTeam: Team
  var rightTeam: Team
  var date: String
  var time: String
  
  init(
    leftTeam: Team,
    rightTeam: Team,
    date: String,
    time: String
  ) {
    self.leftTeam  = leftTeam
    self.rightTeam = rightTeam
    self.date      = date
    self.time      = time
  }
  
  init(json: JSON) {
    self.leftTeam  = Team(json: json["left_team"])
    self.rightTeam = Team(json: json["right_team"])
    self.date      = json["date"].stringValue
    self.time      = json["time"].stringValue
  }
  
  func toParams() -> Api.Params {
    [
      "left_team": [
        "name": self.leftTeam.name,
        "score": self.leftTeam.score
      ],
      "right_team": [
        "name": self.rightTeam.name,
        "score": self.rightTeam.score,
      ],
      "date": self.date,
      "time": self.time
    ]
  }
  
  static func from(params: Api.Params) -> CalendarEvent? {
    do {
      let jsonData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
      let json = try JSON(data: jsonData)
      return CalendarEvent(json: json)
    } catch {
      print(error.localizedDescription)
      return nil
    }
  }
}

// MARK : - Equatable

extension CalendarEvent: Equatable {
  
  static func == (lhs: CalendarEvent, rhs: CalendarEvent) -> Bool {
    lhs.leftTeam == rhs.leftTeam &&
    lhs.rightTeam == rhs.rightTeam &&
    lhs.date == rhs.date &&
    lhs.time == rhs.time
  }
}
