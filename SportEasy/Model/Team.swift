import Foundation
import SwiftyJSON

class Team {
  var name: String
  var score: Int
  
  init(
    name: String,
    score: Int
  ) {
    self.name  = name
    self.score = score
  }
  
  init(json: JSON) {
    self.name  = json["name"].stringValue
    self.score = json["score"].intValue
  }
}

extension Team: Equatable {
  
  static func == (lhs: Team, rhs: Team) -> Bool {
    lhs.name == rhs.name && lhs.score == rhs.score
  }
}
