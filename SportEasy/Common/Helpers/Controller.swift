import Foundation
import RxCocoa
import RxSwift
import UIKit

class Controller: UIViewController {
  
  let bag = DisposeBag()
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    guard let navController = navigationController else {
      return
    }
    navController.navigationBar.isTranslucent = false
    navController.navigationBar.isOpaque = true
    navController.navigationBar.setBackgroundImage(R.image.blueSE(), for: .default)
    navController.navigationBar.backgroundColor = .blueSE
    navController.navigationBar.tintColor = .white
    navController.navigationBar.barTintColor = .blueSE
    self.navigationController?.navigationBar.titleTextAttributes =
    [NSAttributedString.Key.foregroundColor: UIColor.white,
     NSAttributedString.Key.font: UIFont(name: "Helvetica-Bold", size: 17)!]
  }
}
