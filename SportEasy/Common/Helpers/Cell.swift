import Foundation
import UIKit

class Cell : UITableViewCell {
  
  internal init() {
    let reuseIdentifier = String(describing: Self.self)
    super.init(style: .default, reuseIdentifier: reuseIdentifier)
  }

  required init?(coder: NSCoder) { fatalError() }
}
