import Foundation
import RxCocoa
import RxSwift
import UIKit

class ViewModel {
  let bag = DisposeBag()
  
  var loadingViewIsVisibleRelay = BehaviorRelay<Bool>(value: false)
  var loadingViewIsVisible: Observable<Bool> { loadingViewIsVisibleRelay.asObservable() }
}
