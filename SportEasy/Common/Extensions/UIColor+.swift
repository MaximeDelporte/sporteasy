import Foundation
import UIKit

extension UIColor {
  static let blueSE         = UIColor(hex: 0x031447)
  static let backgroundSE   = UIColor(hex: 0xF7F7F7)
  static let greyTextSE     = UIColor(hex: 0x8C929A)
  static let darkGreyTextSE = UIColor(hex: 0x393939)
  static let greenTextSE    = UIColor(red: 99.0 / 255.0, green: 195.0 / 255.0, blue: 123.0 / 255.0, alpha: 1)
  static let redTextSE      = UIColor(hex: 0xFE3824)
}

extension UIColor {
  
  convenience init?(red: Int, green: Int, blue: Int, transparency: CGFloat = 1) {
    guard red >= 0 && red <= 255 else { return nil }
    guard green >= 0 && green <= 255 else { return nil }
    guard blue >= 0 && blue <= 255 else { return nil }

    var trans = transparency
    if trans < 0 { trans = 0 }
    if trans > 1 { trans = 1 }

    self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: trans)
  }
  
  convenience init?(hex: Int, transparency: CGFloat = 1) {
    var trans = transparency
    if trans < 0 { trans = 0 }
    if trans > 1 { trans = 1 }

    let red = (hex >> 16) & 0xff
    let green = (hex >> 8) & 0xff
    let blue = hex & 0xff
    self.init(red: red, green: green, blue: blue, transparency: trans)
  }
}
