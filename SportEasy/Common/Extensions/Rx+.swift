import Foundation
import RxCocoa
import RxSwift

extension ObservableType {
  func subscribeNext(_ callback: @escaping (Element)->Void) -> Disposable {
    subscribe(onNext: callback)
  }
  
  static func next(_ element: Element) -> Observable<Element> {
    Observable.create { observer in
      observer.onNext(element)
      return Disposables.create()
    }
  }
}
