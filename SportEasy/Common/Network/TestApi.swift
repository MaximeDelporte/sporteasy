import Foundation
import RxCocoa
import RxSwift

class TestApi: ApiBase, ApiStrategy {
  
  static var requests  : [ApiRequest] = []
  static var responses : [ApiResponse] = []

  static func clear() {
    requests.removeAll()
    responses.removeAll()
  }

  func request(_ request: ApiRequest) -> Observable<ApiResponse> {
    TestApi.requests.append(request)

    if let response = TestApi.responses.popLast() {
      return .next(response)
    } else {
      return .never()
    }
  }
}
