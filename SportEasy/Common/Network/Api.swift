import Alamofire
import RxCocoa
import RxSwift
import SwiftyJSON

enum Api {
  typealias Method   = Alamofire.HTTPMethod
  typealias Headers  = [String:String]
  typealias Params   = Alamofire.Parameters
  typealias Callback = (ApiResponse)->Void
  typealias Encoding = Alamofire.ParameterEncoding
  
  static var main: ApiStrategy = NetworkApi(baseUrl: "https://hr2v36jyr7.execute-api.eu-west-3.amazonaws.com")
}

protocol ApiStrategy {
  var baseUrl : String { get set }
  func request(_ request: ApiRequest) -> Observable<ApiResponse>
}

class ApiBase {
  var baseUrl : String
  var sharedHeaders : Api.Headers = [:]

  init(baseUrl: String) {
    self.baseUrl = baseUrl
  }
}

class ApiRequest {
  var method : Api.Method
  var path: String
  var headers : Api.Headers
  var params : Api.Params
  var encoding : Api.Encoding

  init(
    _ method: Api.Method,
    _ path: String,
    params: Api.Params = [:],
    encoding: Api.Encoding = URLEncoding(destination: .methodDependent),
    headers: Api.Headers = [:]
  ) {
    self.method = method
    self.path = path
    self.params = params
    self.encoding = encoding
    self.headers = headers
  }
}

class ApiResponse {
  var code: Int?
  var body: String
  var error: AFError?
  
  lazy var json : JSON = {
    JSON(parseJSON: body)
  }()
  
  init(
    code: Int? = nil,
    body: String = "",
    error: AFError? = nil
  ) {
    self.code = code
    self.body = body
    self.error = error
  }
}
