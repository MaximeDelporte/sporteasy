import Alamofire
import Foundation
import RxCocoa
import RxSwift

class NetworkApi: ApiBase, ApiStrategy {
  
  func request(_ request: ApiRequest) -> Observable<ApiResponse> {
    guard let url = URL(string: baseUrl)?.appendingPathComponent(request.path) else {
      return .just(ApiResponse(error: AFError.invalidURL(url: request.path)))
    }
    
    return Observable.create { observer in
      let afRequest = AF
        .request(url, method: request.method, parameters: request.params, encoding: request.encoding)
        .responseString { response in
          let statusCode = response.response?.statusCode
          
          var apiResponse: ApiResponse!
          
          if response.error != nil {
            apiResponse = ApiResponse(code: statusCode, error: response.error)
          }
          
          guard let data = response.data,
                let body = String(data: data, encoding: .utf8) else {
            return
          }
          
          apiResponse = ApiResponse(code: statusCode, body: body)
          observer.onNext(apiResponse)
      }
      
      return Disposables.create {
        afRequest.cancel()
      }
    }
  }
}
