import Foundation
import UIKit

enum Label {
  
  private static func build(textColor: UIColor?, fontName: String = "Helvetica", fontSize: CGFloat) -> UILabel {
    let label = UILabel()
    label.textColor = textColor
    label.font = UIFont(name: fontName, size: fontSize)
    return label
  }

  static func date() -> UILabel {
    build(textColor: .greyTextSE, fontName: "Helvetica-Bold", fontSize: 15.0)
  }
  
  static func time() -> UILabel {
    build(textColor: .greyTextSE, fontSize: 13.0)
  }
  
  static func team() -> UILabel {
    build(textColor: .darkGreyTextSE, fontName: "Helvetica-Bold", fontSize: 17.0)
  }
}
