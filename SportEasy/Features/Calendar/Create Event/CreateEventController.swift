import Foundation
import RxCocoa
import RxSwift
import UIKit

// TODO: remove fatalError from required init

class CreateEventController: UIViewController {
  
  private let dateTextField = UITextField()
  private let hoursTextField = UITextField()
  private let leftTeamTextField = UITextField()
  private let leftScoreTextField = UITextField()
  private let rightTeamTextField = UITextField()
  private let rightScoreTextField = UITextField()
  private let sendFormButton = UIButton()
  
  let verticalSpacing = 8
  
  let bag = DisposeBag()
  let viewModel = CreateEventViewModel()
  
  var delegate: CalendarListDelegate?
  
  init(delegate: CalendarListDelegate) {
    self.delegate = delegate
    super.init(nibName: nil, bundle: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUpViews()
    setUpConstraints()
    setUpBindings()
  }
  
  required init?(coder: NSCoder) {
    super.init(coder: coder)
  }
}

// MARK: - Common Methods

extension CreateEventController {
  
  private func setUpViews() {
    view.backgroundColor = .backgroundSE
    
    dateTextField.placeholder = "Date de l'évènement"
    hoursTextField.placeholder = "Heure de l'évènement"
    leftTeamTextField.placeholder = "Équipe à domicile"
    leftScoreTextField.keyboardType = .numberPad
    leftScoreTextField.placeholder = "Score"
    rightTeamTextField.placeholder = "Équipe à l'extérieur"
    rightScoreTextField.keyboardType = .numberPad
    rightScoreTextField.placeholder = "Score"
    
    [
      dateTextField,
      hoursTextField,
      leftTeamTextField,
      leftScoreTextField,
      rightTeamTextField,
      rightScoreTextField
    ].forEach {
      $0.layer.borderWidth = 1
      $0.layer.borderColor = UIColor.greyTextSE?.cgColor
    }
    
    sendFormButton.setTitleColor(.greenTextSE, for: .normal)
    sendFormButton.setTitle("Envoyer", for: .normal)
    
    view.addSubviews([
      dateTextField,
      hoursTextField,
      leftTeamTextField,
      leftScoreTextField,
      rightTeamTextField,
      rightScoreTextField,
      sendFormButton
    ])
  }
  
  private func setUpConstraints() {
    dateTextField.snp.makeConstraints {
      $0.top.equalToSuperview().offset(verticalSpacing)
      $0.left.right.equalToSuperview().inset(16)
    }
    
    hoursTextField.snp.makeConstraints {
      $0.top.equalTo(dateTextField.snp.bottom).offset(verticalSpacing)
      $0.left.right.equalTo(dateTextField)
    }
    
    leftTeamTextField.snp.makeConstraints {
      $0.top.equalTo(hoursTextField.snp.bottom).offset(verticalSpacing)
      $0.left.equalTo(dateTextField)
      $0.width.equalToSuperview().multipliedBy(0.7)
    }
    
    leftScoreTextField.snp.makeConstraints {
      $0.top.equalTo(leftTeamTextField)
      $0.left.equalTo(leftTeamTextField.snp.right).offset(4)
      $0.right.equalTo(dateTextField)
    }
    
    rightTeamTextField.snp.makeConstraints {
      $0.top.equalTo(leftTeamTextField.snp.bottom).offset(verticalSpacing)
      $0.left.equalTo(dateTextField)
      $0.width.equalTo(leftTeamTextField)
    }
    
    rightScoreTextField.snp.makeConstraints {
      $0.top.equalTo(rightTeamTextField)
      $0.left.equalTo(rightTeamTextField.snp.right).offset(4)
      $0.right.equalTo(dateTextField)
    }
    
    sendFormButton.snp.makeConstraints {
      $0.top.equalTo(rightTeamTextField.snp.bottom).offset(verticalSpacing)
      $0.centerX.equalToSuperview()
    }
  }
  
  private func setUpBindings() {
    dateTextField.rx.text.bind(to: self.viewModel.dateRelay).disposed(by: bag)
    dateTextField.rx.text.bind(to: viewModel.dateRelay).disposed(by: bag)
    hoursTextField.rx.text.bind(to: viewModel.hoursRelay).disposed(by: bag)
    
    leftTeamTextField.rx.text.bind(to: viewModel.leftTeamRelay).disposed(by: bag)
    leftScoreTextField.rx.text.subscribeNext { value in
      guard let value = value,
            let score = Int(value) else {
        return
      }
      self.viewModel.leftTeamScoreRelay.accept(score)
    }.disposed(by: bag)
    
    rightTeamTextField.rx.text.bind(to: viewModel.rightTeamRelay).disposed(by: bag)
    rightScoreTextField.rx.text.subscribeNext { value in
      guard let value = value,
            let score = Int(value) else {
        return
      }
      self.viewModel.rightTeamScoreRelay.accept(score)
    }.disposed(by: bag)
    
    viewModel.newCalendarEvents.subscribeNext { newEvents in
      guard let newEvents = newEvents else { return }
      self.delegate?.reloadCalendarEvent(with: newEvents)
    }.disposed(by: bag)
    
    sendFormButton.rx.tap.subscribeNext { _ in
      self.viewModel.createCalendarEvent()
    }.disposed(by: bag)
  }
}
