import Foundation
import RxCocoa
import RxSwift

class CreateEventViewModel: ViewModel {
  
  typealias CalendarEventsRequest = (Api.Params) -> Observable<[CalendarEvent]>
  var createCalendarEventsRequest: CalendarEventsRequest
  
  private let calendarEventsRelay = BehaviorRelay<[CalendarEvent]?>(value: nil)
  var newCalendarEvents: Observable<[CalendarEvent]?> { calendarEventsRelay.asObservable() }
  
  private static let createCalendatEventsFromRepository: CalendarEventsRequest = { params in
    CalendarRepository().createCalendarEvent(with: params)
  }
  
  var dateRelay = BehaviorRelay<String?>(value: nil)
  var hoursRelay = BehaviorRelay<String?>(value: nil)
  var leftTeamRelay = BehaviorRelay<String?>(value: nil)
  var leftTeamScoreRelay = BehaviorRelay<Int?>(value: nil)
  var rightTeamRelay = BehaviorRelay<String?>(value: nil)
  var rightTeamScoreRelay = BehaviorRelay<Int?>(value: nil)
  
  override init() {
    createCalendarEventsRequest = CreateEventViewModel.createCalendatEventsFromRepository
  }
}

// MARK: - Convenience Methods

extension CreateEventViewModel {
  
  func createCalendarEvent() {
    let date = dateRelay.value ?? ""
    let time = hoursRelay.value ?? ""
    let leftTeam = Team(name: leftTeamRelay.value ?? "", score: leftTeamScoreRelay.value ?? 0)
    let rightTeam = Team(name: rightTeamRelay.value ?? "", score: rightTeamScoreRelay.value ?? 0)
    
    let calendarEvent = CalendarEvent(leftTeam: leftTeam, rightTeam: rightTeam, date: date, time: time)
    
    loadingViewIsVisibleRelay.accept(true)
    createCalendarEventsRequest(calendarEvent.toParams()).subscribeNext { events in
      self.loadingViewIsVisibleRelay.accept(false)
      self.calendarEventsRelay.accept(events)
    }.disposed(by: bag)
  }
}
