import UIKit
import RxCocoa
import RxSwift
import SnapKit

protocol CalendarListDelegate {
  func reloadCalendarEvent(with newEvents: [CalendarEvent])
}

class CalendarViewController: Controller {
  
  private let tableView = UITableView()
  private let refreshControl = UIRefreshControl()
  private let loadingView = UIActivityIndicatorView()
  
  private let viewModel = CalendarViewModel()

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    title = R.string.calendar.title()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setUpViews()
    setUpConstraints()
    setUpBindings()
  }
}

// MARK: - Common Methods

extension CalendarViewController {
  
  private func setUpViews() {
    let rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(createEventForm))
    navigationItem.rightBarButtonItem = rightBarButtonItem

    view.backgroundColor = .backgroundSE
    
    tableView.separatorStyle = .none
    tableView.backgroundColor = .clear
    tableView.addSubview(refreshControl)
    tableView.delegate = self
    tableView.dataSource = self
    
    loadingView.hidesWhenStopped = true
    
    view.addSubviews([tableView, loadingView])
  }
  
  private func setUpConstraints() {
    tableView.snp.makeConstraints {
      $0.left.top.right.equalToSuperview()
      $0.bottom.equalTo(view.safeAreaLayoutGuide)
    }
    
    loadingView.snp.makeConstraints {
      $0.centerX.centerY.equalToSuperview()
    }
  }
  
  private func setUpBindings() {
    refreshControl.addTarget(self, action: #selector(loadCalendarEvents), for: .valueChanged)
    
    viewModel.calendarEvents.subscribeNext { calendarEvents in
      self.refreshControl.endRefreshing()
      self.tableView.reloadData()
    }.disposed(by: bag)
    
    viewModel.loadingViewIsVisible.bind(to: loadingView.rx.isAnimating).disposed(by: bag)
    
    viewModel.loadCalendarEvents()
  }
}

// MARK: - CalendarListDelegate

extension CalendarViewController: CalendarListDelegate {
  
  func reloadCalendarEvent(with newEvents: [CalendarEvent]) {
    viewModel.reloadCalendarEvent(with: newEvents)
    navigationController?.popViewController(animated: true)
    
    DispatchQueue.main.async {
      let indexPath = IndexPath(row: newEvents.count - 1, section: 0)
      self.tableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
    }
  }
}

// MARK: - Convenience Methods

extension CalendarViewController {
  
  @objc
  private func loadCalendarEvents() {
    viewModel.loadCalendarEvents()
  }
  
  @objc
  private func createEventForm() {
    navigationController?.pushViewController(CreateEventController(delegate: self), animated: true)
  }
}

// MARK: - UITableViewDelegate

extension CalendarViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    12.0
  }
  
  func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: 12.0))
  }
}

// MARK: - UITableViewDataSource

extension CalendarViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    viewModel.numberOfRows()
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let currentCalendarEvent = viewModel.getCalendarEvent(atRow: indexPath.row) else {
      return UITableViewCell()
    }
    
    let cell = CalendarEventCell()
    cell.configureCell(with: currentCalendarEvent)
    
    return cell
  }
}

