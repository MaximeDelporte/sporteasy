import Foundation
import RxCocoa
import RxSwift
import UIKit

class CalendarViewModel: ViewModel {
  
  typealias CalendarEventsRequest = () -> Observable<[CalendarEvent]>
  var calendarEventsRequest: CalendarEventsRequest
  
  private let calendarEventsRelay = BehaviorRelay<[CalendarEvent]?>(value: nil)
  var calendarEvents: Observable<[CalendarEvent]?> { calendarEventsRelay.asObservable() }
  
  private static let calendarEventsFromRepository: CalendarEventsRequest = {
    CalendarRepository().getCalendarEvents()
  }
  
  override init() {
    calendarEventsRequest = CalendarViewModel.calendarEventsFromRepository
    super.init()
  }
}

// MARK: - Convenience Methods

extension CalendarViewModel {
  
  func numberOfRows() -> Int {
    guard let calendarEvents = calendarEventsRelay.value else { return 0 }
    return calendarEvents.count
  }
  
  func getCalendarEvent(atRow row: Int) -> CalendarEvent? {
    guard let calendarEvents = calendarEventsRelay.value else { return nil }
    return calendarEvents[row]
  }
}

// MARK: - Network Method

extension CalendarViewModel {
  
  @objc
  func loadCalendarEvents() {
    loadingViewIsVisibleRelay.accept(true)
    
    calendarEventsRequest().subscribeNext { calendarEvents in
      self.loadingViewIsVisibleRelay.accept(false)
      self.calendarEventsRelay.accept(calendarEvents)
    }.disposed(by: bag)
  }
  
  func reloadCalendarEvent(with newEvents: [CalendarEvent]) {
    self.calendarEventsRelay.accept(newEvents)
  }
}
