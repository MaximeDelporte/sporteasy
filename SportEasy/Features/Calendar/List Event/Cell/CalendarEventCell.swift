import Foundation
import UIKit

class CalendarEventCell: Cell {
  
  private let cellView = UIView()
  private let dateLabel = Label.date()
  private let timeLabel = Label.time()
  private let separator = UIView()
  private let leftTeamLabel = Label.team()
  private let leftTeamScore = Label.team()
  private let rightTeamLabel = Label.team()
  private let rightTeamScore = Label.team()
  
  private let outsideVerticalMargin = 12
  private let outsideHorizontalMargin = 20
  private let horizontalMargin = 16
  private let verticalMargin = 20
  private let spaceBetweenItems = 12
  
  private var calendarEvent: CalendarEvent?
  private var layoutIsSet: Bool = false
  
  override init() {
    super.init()
    setUpViews()
    setUpConstraints()
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    if layoutIsSet { return }
    
    cellView.layer.cornerRadius = 20.0
    cellView.layer.shadowOpacity = 0.2
    cellView.layer.shadowRadius = 5.0
    cellView.layer.shadowOffset = CGSize.zero
    cellView.layer.shadowColor = UIColor.darkGreyTextSE?.cgColor
    
    [leftTeamScore, rightTeamScore].forEach { view in
      view.snp.makeConstraints {
        $0.width.equalTo(leftTeamScore.intrinsicContentSize.width)
      }
    }
    
    layoutIsSet = true
  }
  
  required init?(coder: NSCoder) {
    fatalError("CalendarEventCell - required init fatalError")
  }
}

// MARK: - Common Methods

extension CalendarEventCell {
  
  private func setUpViews() {
    backgroundColor = .clear
    selectionStyle = .none
    cellView.backgroundColor = .white
    separator.backgroundColor = .greyTextSE
    
    cellView.addSubviews([
      dateLabel,
      timeLabel,
      separator,
      leftTeamLabel,
      leftTeamScore,
      rightTeamLabel,
      rightTeamScore
    ])
    
    contentView.addSubview(cellView)
  }
  
  private func setUpConstraints() {
    cellView.snp.makeConstraints {
      $0.top.equalToSuperview().offset(outsideHorizontalMargin)
      $0.left.equalToSuperview().offset(outsideHorizontalMargin)
      $0.right.equalToSuperview().offset(-outsideHorizontalMargin)
      $0.bottom.equalToSuperview().offset(-outsideVerticalMargin)
    }
    
    dateLabel.snp.makeConstraints {
      $0.top.equalToSuperview().offset(24)
      $0.left.equalToSuperview().offset(horizontalMargin)
    }
    
    timeLabel.snp.makeConstraints {
      $0.bottom.equalTo(dateLabel)
      $0.right.equalToSuperview().offset(-horizontalMargin)
    }
    
    separator.snp.makeConstraints {
      $0.top.equalTo(dateLabel.snp.bottom).offset(spaceBetweenItems)
      $0.left.equalTo(dateLabel)
      $0.right.equalTo(timeLabel)
      $0.height.equalTo(0.5)
    }
    
    leftTeamLabel.snp.makeConstraints {
      $0.top.equalTo(separator.snp.bottom).offset(spaceBetweenItems)
      $0.left.equalTo(separator)
      $0.right.equalTo(leftTeamScore.snp.left).offset(-2).priority(.required)
      $0.bottom.equalTo(rightTeamLabel.snp.top).offset(-8.0)
    }
    
    leftTeamScore.snp.makeConstraints {
      $0.bottom.equalTo(leftTeamLabel)
      $0.right.equalTo(separator)
    }
    
    rightTeamLabel.snp.makeConstraints {
      $0.left.equalTo(leftTeamLabel)
      $0.right.equalTo(rightTeamScore.snp.left).offset(-2).priority(.required)
      $0.bottom.equalToSuperview().offset(-24)
    }
    
    rightTeamScore.snp.makeConstraints {
      $0.bottom.equalTo(rightTeamLabel)
      $0.right.equalTo(separator)
    }
  }
}

// MARK: - Convenience Methods

extension CalendarEventCell {
  
  func configureCell(with calendarEvent: CalendarEvent) {
    self.calendarEvent = calendarEvent
    dateLabel.text = calendarEvent.date
    timeLabel.text = calendarEvent.time
    
    leftTeamLabel.text = calendarEvent.leftTeam.name
    leftTeamScore.text = String(calendarEvent.leftTeam.score)
    leftTeamScore.textColor = leftScoreColor()
    
    rightTeamLabel.text = calendarEvent.rightTeam.name
    rightTeamScore.text = String(calendarEvent.rightTeam.score)
    rightTeamScore.textColor = rightScoreColor()
  }
  
  private func leftScoreColor() -> UIColor? {
    guard let event = calendarEvent else { return .greyTextSE }
    return getColorFor(team: event.leftTeam, comparedTo: event.rightTeam)
  }
  
  private func rightScoreColor() -> UIColor? {
    guard let event = calendarEvent else { return .greyTextSE }
    return getColorFor(team: event.rightTeam, comparedTo: event.leftTeam)
  }
  
  func getColorFor(team: Team, comparedTo otherTeam: Team) -> UIColor? {
    if team.score > otherTeam.score {
      return .greenTextSE
    } else if team.score < otherTeam.score {
      return .redTextSE
    } else {
      return .greyTextSE
    }
  }
}
