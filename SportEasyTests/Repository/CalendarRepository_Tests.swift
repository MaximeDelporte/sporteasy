import Alamofire
import RxBlocking
import XCTest

@testable import SportEasy

class CalendarRepository_Tests: TestCase {
  
  let repository = CalendarRepository()
  
  func test_call_calendar_event_should_retrieve_correct_data() {
    // arrange
    stubNextApiResponse(code: 200, fixture: "get-calendar-events-success")
    
    // act
    let calendarEvents = try! repository.getCalendarEvents().toBlocking().first()!
    
    // assert
    XCTAssertNotNil(calendarEvents)
    
    let testedCalendarEvents = CalendarEventFactory.getCalendarEvents()
    for (index, testedCalendarEvent) in testedCalendarEvents.enumerated() {
      current(calendarEvent: calendarEvents[index], shouldBeEqualTo: testedCalendarEvent)
    }
    
    XCTAssertEqual(TestApi.requests.count, 1)
    let request = TestApi.requests.first!
    XCTAssertEqual(request.path, "/default/events")
  }
  
  func test_when_create_event_then_repository_should_return_event_create_response() {
    // arrange
    stubNextApiResponse(code: 200, fixture: "post-calendar-event")
    
    // act
    let params: [String: Any] = [
      "left_team": [
        "name": "Angleterre",
        "score": 1
      ],
      "right_team": [
        "name": "Italie",
        "score": 1
      ],
      "date": "Dimanche 11 Juillet",
      "time": "21:00"
    ]
    
    let eventCreationResponse = try! repository.createCalendarEvent(with: params).toBlocking().first()!
    
    guard let lastEvent = eventCreationResponse.last,
          let calendarEventFromParams = CalendarEvent.from(params: params) else {
      XCTFail("lastEvent shouldn't be nil or calendarEventFromParams fail")
      return
    }
    
    // assert
    XCTAssertEqual(lastEvent, calendarEventFromParams)
  }
}

// MARK: - Convenience Methods

extension CalendarRepository_Tests {
  
  private func current(calendarEvent: CalendarEvent, shouldBeEqualTo testedCalendarEvent: CalendarEvent) {
    XCTAssertEqual(calendarEvent.leftTeam.name, testedCalendarEvent.leftTeam.name)
    XCTAssertEqual(calendarEvent.leftTeam.score, testedCalendarEvent.leftTeam.score)
    XCTAssertEqual(calendarEvent.rightTeam.name, testedCalendarEvent.rightTeam.name)
    XCTAssertEqual(calendarEvent.rightTeam.score, testedCalendarEvent.rightTeam.score)
    XCTAssertEqual(calendarEvent.date, testedCalendarEvent.date)
    XCTAssertEqual(calendarEvent.time, testedCalendarEvent.time)
  }
}
