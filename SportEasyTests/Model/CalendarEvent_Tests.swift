import XCTest

@testable import SportEasy

class CalendarEvent_Tests: TestCase {

  func test_when_call_to_params_methods_from_calendar_event_then_it_should_return_correct_params() {
    // arrange
    let calendarEvent = CalendarEventFactory.getEventCreationResponse().first!
    
    // act
    let params = calendarEvent.toParams()
    
    // assert
    let calendarEventFromParams = CalendarEvent.from(params: params)
    XCTAssertEqual(calendarEvent, calendarEventFromParams)
  }
}
