import Foundation
import XCTest

@testable import SportEasy

class CalendarEventCell_Tests: TestCase {
  
  let cell = CalendarEventCell()
  
  // Winner
  let winnerTeam1 = Team(name: "Winner 1", score: 5)
  let winnerTeam2 = Team(name: "Winner 2", score: 5)
  
  // Looser
  let looserTeam1 = Team(name: "Looser 1", score: 0)
  
  func test_get_score_color() {
    // Test winner
    let winnerColor = cell.getColorFor(team: winnerTeam1, comparedTo: looserTeam1)
    XCTAssertEqual(winnerColor, UIColor.greenTextSE)
    
    // Test looser
    let looserColor = cell.getColorFor(team: looserTeam1, comparedTo: winnerTeam1)
    XCTAssertEqual(looserColor, UIColor.redTextSE)
    
    // Test draw
    let drawColor = cell.getColorFor(team: winnerTeam1, comparedTo: winnerTeam2)
    XCTAssertEqual(drawColor, UIColor.greyTextSE)
  }
}
