import Foundation
import RxCocoa
import RxSwift
import RxBlocking
import XCTest

@testable import SportEasy

class CalendarViewModel_Tests: TestCase {
  
  let vm = CalendarViewModel()
  var calendarEventsMock: [CalendarEvent]!
  
  // Subscribed properties
  
  var calendarEvents = BehaviorRelay<[CalendarEvent]?>(value: nil)
  var loadingViewIsVisible: [Bool] = []
 
  override func setUp() {
    super.setUp()
    let json = getJSON(fromFile: "get-calendar-events-success")
    let eventsJson = json["events"]
    calendarEventsMock = eventsJson.arrayValue.map { CalendarEvent(json: $0) }
  }
  
  func test_load_calendar_events() {
    // arrange
    vm.calendarEventsRequest = { .next(self.calendarEventsMock) }
    subscribe()
    
    // act
    vm.loadCalendarEvents()
    
    // assert
    let testedCalendarEvents = CalendarEventFactory.getCalendarEvents()
    for (index, testedCalendarEvent) in testedCalendarEvents.enumerated() {
      current(calendarEvent: calendarEvents.value![index], shouldBeEqualTo: testedCalendarEvent)
    }
    
    XCTAssertEqual(loadingViewIsVisible, [false, true, false])
  }
  
  func test_number_of_rows_when_retrieve_calendar_events() {
    // arrange
    vm.calendarEventsRequest = { .next(self.calendarEventsMock) }
    subscribe()
    
    // act
    vm.loadCalendarEvents()
    
    // assert
    XCTAssertEqual(vm.numberOfRows(), 12)
  }
  
  func test_cell_for_row_at_index_path_when_retrieve_calendar_events() {
    // arrange
    vm.calendarEventsRequest = { .next(self.calendarEventsMock) }
    subscribe()
    
    // act
    vm.loadCalendarEvents()
    
    // assert
    guard let firstCalendarEvent = vm.getCalendarEvent(atRow: 0) else {
      XCTFail("firstCalendarEvent should exist")
      return
    }
    XCTAssertEqual(firstCalendarEvent.leftTeam.name, "FC Olympiens")
    XCTAssertEqual(firstCalendarEvent.leftTeam.score, 2)
    XCTAssertEqual(firstCalendarEvent.rightTeam.name, "FC Sèvres")
    XCTAssertEqual(firstCalendarEvent.rightTeam.score, 1)
    XCTAssertEqual(firstCalendarEvent.date, "Samedi 4 octobre")
    XCTAssertEqual(firstCalendarEvent.time, "17:30")
  }
}

// MARK: - Convenience Methods

extension CalendarViewModel_Tests {
  
  private func subscribe() {
    vm.calendarEvents
      .bind(to: self.calendarEvents)
      .disposed(by: bag)
    
    vm.loadingViewIsVisibleRelay.subscribeNext { event in
      self.loadingViewIsVisible.append(event)
    }.disposed(by: bag)
  }
  
  private func current(calendarEvent: CalendarEvent, shouldBeEqualTo testedCalendarEvent: CalendarEvent) {
    XCTAssertEqual(calendarEvent.leftTeam.name, testedCalendarEvent.leftTeam.name)
    XCTAssertEqual(calendarEvent.leftTeam.score, testedCalendarEvent.leftTeam.score)
    XCTAssertEqual(calendarEvent.rightTeam.name, testedCalendarEvent.rightTeam.name)
    XCTAssertEqual(calendarEvent.rightTeam.score, testedCalendarEvent.rightTeam.score)
    XCTAssertEqual(calendarEvent.date, testedCalendarEvent.date)
    XCTAssertEqual(calendarEvent.time, testedCalendarEvent.time)
  }
}
