import Foundation

@testable import SportEasy

class CalendarEventFactory {
  
  static func getCalendarEvents() -> [CalendarEvent] {
    [
      CalendarEvent(
        leftTeam: Team(name: "FC Olympiens", score: 2),
        rightTeam: Team(name: "FC Sèvres", score: 1),
        date: "Samedi 4 octobre", time: "17:30"
      ),
      CalendarEvent(
        leftTeam: Team(name: "Rungis", score: 0),
        rightTeam: Team(name: "FC Olympiens", score: 2),
        date: "Dimanche 12 octobre", time: "18:30"
      ),
      CalendarEvent(
        leftTeam: Team(name: "FC Olympiens", score: 0),
        rightTeam: Team(name: "Rueil Malmaison", score: 3),
        date: "Samedi 18 octobre", time: "14:00"
      ),
      CalendarEvent(
        leftTeam: Team(name: "FC Bures", score: 1),
        rightTeam: Team(name: "FC Olympiens", score: 1),
        date: "Samedi 25 octobre", time: "20:00"
      ),
      CalendarEvent(
        leftTeam: Team(name: "FC Olympiens", score: 1),
        rightTeam: Team(name: "Les petit anges", score: 0),
        date: "Samedi 1 novembre", time: "17:30"
      ),
      CalendarEvent(
        leftTeam: Team(name: "FC Thiais", score: 2),
        rightTeam: Team(name: "FC Olympiens", score: 2),
        date: "Samedi 8 novembre", time: "19:00"
      ),
    ]
  }
  
  static func getEventCreationResponse() -> [CalendarEvent] {
    [
      CalendarEvent(
        leftTeam: Team(name: "Angleterre", score: 1),
        rightTeam: Team(name: "Italie", score: 1),
        date: "Dimanche 11 Juillet", time: "21:00"
      ),
    ]
  }
}
