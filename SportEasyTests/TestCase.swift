import Foundation
import RxCocoa
import RxSwift
import SwiftyJSON
import XCTest

@testable import SportEasy

class TestCase: XCTestCase {
  
  let bag = DisposeBag()
  
  override func setUp() {
    super.setUp()
    continueAfterFailure = false
    Api.main = TestApi(baseUrl: "http://localhost:3000/")
    TestApi.clear()
  }
  
  func stubNextApiResponse(code: Int, fixture: String = "") {
    let response = ApiResponse(code: code, body: fixtureContent(fixture))
    TestApi.responses.insert(response, at: 0)
  }
  
  private func fixtureContent(_ id: String) -> String {
    let bundle = Bundle(for: type(of: self))
    let url    = bundle.url(forResource: id, withExtension: "json")!
    let data   = try! Data(contentsOf: url)
    return data.string!
  }
  
  func getJSON(fromFile path: String) -> JSON {
    let bundle = Bundle(for: type(of: self))
    let url    = bundle.url(forResource: path, withExtension: "json")!
    let data   = try! Data(contentsOf: url)
    return JSON(parseJSON: data.string!)
  }
}

